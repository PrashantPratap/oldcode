
using System.IO;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Azure.WebJobs;
using Microsoft.Azure.WebJobs.Extensions.Http;
using Microsoft.AspNetCore.Http;
using Microsoft.Azure.WebJobs.Host;
using Newtonsoft.Json;
using System.Reflection;
using System;

namespace OldCodeIssue
{
    public static class Function2
    {
        [FunctionName("Function2")]
        public static IActionResult Run([HttpTrigger(AuthorizationLevel.Anonymous, "get", "post", Route = null)]HttpRequest req, TraceWriter log)
        {
            
            log.Info("C# HTTP trigger function processed a request.");

            var machineName = Environment.GetEnvironmentVariable("COMPUTERNAME");
            var instanceID = Environment.GetEnvironmentVariable("WEBSITE_INSTANCE_ID");

            int pid = System.Diagnostics.Process.GetCurrentProcess().Id;
            int domainid = System.AppDomain.CurrentDomain.Id;
            

            Assembly thisAssem = typeof(Function2).Assembly;
            AssemblyName thisAssemName = thisAssem.GetName();
            DateTime dllTime = System.IO.File.GetLastWriteTime(thisAssem.Location);


            var dllversion = $"Version is 5 : time is {dllTime} ; PID is {pid} ; AppDomainID is {domainid} ; machineName is {machineName} ; instanceID is {instanceID} ; DLL Name is {thisAssemName} ;";

            log.Info(dllversion);

            return (ActionResult)new OkObjectResult(dllversion);
                
        }
    }
}
