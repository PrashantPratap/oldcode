using System;
using Microsoft.Azure.WebJobs;
using Microsoft.Azure.WebJobs.Host;

namespace OldCodeIssue
{
    public static class Function1
    {
        [FunctionName("Function1")]
        public static void Run([TimerTrigger("0/5 * * * * *")]TimerInfo myTimer, TraceWriter log)
        {
            log.Info($"C# Timer trigger function executed at: {DateTime.Now}");
            System.Threading.Thread.Sleep(10 * 1000); // sleep 10 seconds


            return;
                
        }
    }
}
